import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component'
import { LoginComponent } from './page/login/login.component'

const routes: Routes =  [
							{
								'path' : 'login',
								'component' : LoginComponent
							},

							{
								'path' : '',
								'loadChildren' : () => import('./layout/main/main.module').then(set_Module => set_Module.MainModule)
							}
						]

@NgModule  ({
				declarations: [AppComponent],
				imports:
				[
					BrowserModule,
					CommonModule,
					RouterModule.forRoot(routes)
				],
				'bootstrap' : [AppComponent]
			})

export class AppModule { }