import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component	({
				selector: 'app-main',
				templateUrl: './main.component.html',
				styleUrls: ['./main.component.scss']
			})

export class MainComponent implements OnInit, AfterViewInit
{

	constructor() { }

	public ngOnInit(): void
	{

	}

	public ngAfterViewInit() : void
	{
		require('src/assets/js/startmin.js')
	}
}
