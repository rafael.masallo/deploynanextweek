import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, Routes } from '@angular/router'

//import { SidebarModule } from '../sidebar/sidebar.module'

import { MainComponent } from './main.component'
import { DashboardComponent } from '../../page/dashboard/dashboard.component'

import { HeaderComponent } from '../header/header.component'
import { SidebarComponent } from '../sidebar/sidebar.component'

const routes: Routes =  [
							{
								'path' : '',
								'component' : MainComponent,
								'children' :
								[
									{
										path: '',
										redirectTo: 'dashboard'
									},

									{
										'path' : 'product',
										'loadChildren' : () => import('../../page/product/product.module').then(set_Module => set_Module.ProductModule)
									},

									{
										'path' : 'dashboard',
										'component' : DashboardComponent
									}
								]
							}
						]

@NgModule	({
				'declarations'	:
				[
					MainComponent, HeaderComponent, SidebarComponent
				],

				'imports'		:
				[
					//SidebarModule,
					RouterModule.forChild(routes)
				]
			})

export class MainModule { }
