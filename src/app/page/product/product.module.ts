import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'

import { ProductComponent } from './product.component'
import { ManageComponent } from './manage/manage.component'
import { ListComponent } from './list/list.component'

const routes: Routes =  [
							{
								'path' : '',
								'redirectTo' : 'list',
								'pathMatch' : 'full'
							},
							{
								'path' : 'list',
								'component' : ListComponent
							},

							{
								'path' : 'manage',
								'component' : ManageComponent
							}
						]

@NgModule	({
				declarations: [ProductComponent],
				imports:
				[
					CommonModule,
					RouterModule.forChild(routes)
				]
			})

export class ProductModule { }
