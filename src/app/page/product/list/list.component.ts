import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component	({
				selector: 'app-list',
				templateUrl: './list.component.html',
				styleUrls: ['./list.component.scss']
			})
export class ListComponent implements OnInit, AfterViewInit
{

	constructor() { }

	public ngOnInit() : void
	{
		
	}

	public ngAfterViewInit() : void
	{
		($('#dataTables-example') as any).DataTable
		({
			responsive: true,

			'columnDefs':
			[
				{
					"targets": "_all",
					"className": "text-center"
				}
			],
		});
	}
}
